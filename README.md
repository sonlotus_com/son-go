## **GIỚI THIỆU CHUNG​**
Công ty TNHH SX TM DV Bích Trang được thành lập vào năm 2015, hoạt động trong lĩnh vực sản xuất và kinh doanh các sản phẩm sơn, keo, vật liệu và hóa chất trong ngành xây dựng. Khởi đầu, Bích Trang được biết đến như chuyên gia hàng đầu trong việc cung cấp giải pháp xử lý mối nối cho các tấm fiber cement và là cty tiên phong trong sản xuất sơn giả gỗ gốc nước.

Sau nhiều nỗ lực nghiên cứu, phát triển, tạo dựng uy tín và chất lượng sản phẩm, Bích Trang đã và đang được nhiều khách hàng tín nhiệm với hai thương hiệu được khẳng định trên thị trường là [Jade’s Solution](https://jadessolutions.com) và [Sơn Lotus](https://sonlotus.com). Trong đó, thương hiệu Jade’s Solution tập trung vào các sản phẩm keo, chống thấm, vật liệu thi công và hóa chất xây dựng. Và thương hiệu Sơn Lotus bao gồm các sản phẩm sơn gốc nước ứng dụng trong [sơn giả gỗ](https://sonlotus.com/product-category/san-pham/son-gia-go/), [sơn gỗ](https://sonlotus.com/product-category/san-pham/son-go-he-nuoc/), sơn ngói, [sơn kim loại](https://sonlotus.com/product-category/san-pham/son-kim-loai-he-nuoc/), sàn…định hướng an toàn với người tiêu dùng và thân thiện môi trường ,thay thế các sản phẩm hệ dung môi.

Các sản phẩm của cty Bích Trang đã và đang được phân phối rộng khắp cả nước; và được sử dụng tại nhiều công trình trong và ngoài nước.

## TẦM NHÌN​
* Là chuyên gia hàng đầu về các sản phẩm sơn thân thiện môi trường và an toàn cho người sử dụng
* Cty đầu ngành trong việc sản xuất và kinh doanh các sản phẩm keo, sơn, vật liệu, hóa chất và phụ gia trong xây dựng
## GIÁ TRỊ CỐT LÕI
* Chất lượng: Thực thi cam kết với các đối tác, khách hàng và nguời tiêu dùng về mục tiêu nâng cao chất lượng sản phẩm, chất lượng
* Uy tín: Cung cấp sản phẩm và dịch vụ chăm sóc khách hàng tốt nhất. Sự an tâm, tin tưởng và hài lòng của khách hàng là mục tiêu trong tất cả các hoạt động của Bích Trang
* Đột phá trong giải pháp và công nghệ: luôn luôn khám phá những công nghệ mới, từ đó nghiên cứu và sản xuất những sản phẩm có ứng dụng thực tiễn, mang lại tiện ích và tối đa giá trị cho khách hàng
* Sản phẩm thân thiện với người tiêu dùng và môi trường: tập trung nghiên cứu và sản xuất các sản phẩm sơn gốc nước với công nghệ vượt trội để đảm bảo an toàn với người tiêu dùng và thân thiện môi trường
* Phát triển bền vững: luôn luôn lắng nghe các phản hồi của khách hàng, từ đó đưa ra những giải pháp và giá trị bền vững và lâu dài
* Mang đến giá trị và sự hài long cho khách hàng nhât
* Là công ty hàng đầu trong nghiên cứu và ứng dụng những công nghệ mới và đột phá nhất để đưa ra những sản phẩm tốt nhất cho khách hàng